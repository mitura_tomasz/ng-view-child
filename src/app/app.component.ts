import { Component, ViewChild, onInit, AfterViewInit, ChangeDetectorRef } from '@angular/core';

import { TodoComponent } from './components/todo/todo.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})  
export class AppComponent implements ngOnInit, AfterViewInit {
  
  todo: string;
  @ViewChild(TodoComponent) todoViewChild: TodoComponent;
  
  constructor(private _changeDetectorRef: ChangeDetectorRef) {}
  
  ngOnInit() {
    this.todo = 'Do homework';
  }
  
  ngAfterViewInit() {
    this.todoViewChild.title = 'Amelinium szelesci';
    this._changeDetectorRef.detectChanges();
  }
}
